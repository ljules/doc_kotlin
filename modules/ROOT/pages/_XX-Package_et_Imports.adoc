// Activation de la numérotaion des section :
:sectnums:
//
// Attributs du sommaire :
:toc: left
:toclevels: 5
:toc-title: Sommaire
//
// Localisation des images :
:imagesdir: ./assets/images
//
// Option d'affichage des codes sources :
:source-highlighter: pygments
:pygments-style: manni
:pygments-linenums-mode: inline



== Principes des packages
=== Rappel sur les classes et fichiers

Les packages permettent d'organiser les classes et fonction en groupes et sous groupes, cela permet :

* Prévenir les conflits de noms, plusieurs classes pourront avoir le même identifiant si elles sont disposées dans des packages différents. Le conflit est éviter grâce à l'espace de nom.
* Assurer des régles de visiblité selon l'apartenance ou pas à un package.

==== Package ou paquetage

Dans cette documentation nous utiliserons le nom de *package*. Le mot paquetage est la traduction en français du terme anglais package.


==== Rappels du fonctionnement de Java

En Java il est fortement préconisé de de placer 1 classe par fichier. 
Il peut y avoir plusieurs classes dans un fichier mais un fichier ne peut comporter qu'une unique classe publique.
Si un ficiher comporte une classe publique, alors le fichier doit avoir le même nom celui de la classe publique.
Si un ficiher ne comporte pas de classe publique, on peut alors lui donner le nom qu'on veut.
Les extensions des fichiers est .java.

Au moment de la compilation, le compilateur java va générer des fichiers compilés avec l'extension .class. Chaque class aura son fichier .class (même si la classe était dans un fichier source .java comportant plusieurs classes). Chaque fichier .class porte le nom de la classe.

Dans un fichier Java, on doit forcement respecter l'ordre suivant :
 
 * Déclaration de package
 * Déclaration d'import
 * Définition de la classe
 
Toutes les classes placées dans des fichiers ne comportant pas de déclaration de package font implicitement partie d'un package appelé "package par défaut". Les classes placées dans le package par défaut ne sont visibles que depuis ce package.

Les fichiers sources doivent être contenus dans une arborescence de dossiers respectant la hiérarchie des packages. 


=== Rappel sur les fichiers .jar

Afin de faciliter le partage de package, Java propose une format d'archive compressé, le format .jar.


=== Convention de nommage des imports

Les noms de package ne comportent que des minuscules.
Afin d'éviter des conflits (lorsque des packages portent le même nom) , Java conseille de rallonger le nom des package avec le nom de la société ou organisme ou concepteur...
Si on dispose d'un nom de domaine, on l'utilisera mais en inversant son ordre, par exemple "worldcompany.com" devient com.worldcompany.



== Package et Imports

Sans instructions d'importation l'accès au différents éléments (constantes, fonctions, classes, etc) devront se faire en spécifiant complètement le chemin de l'élément.

[source, Kotlin]
.Exemple : Utilisation de la constante PI du package kotlin.math
----
println("Le nombre PI : ${kotlin.math.PI}")
----

=== Importations
Les  importation se font avec le mot clé [red]*import*.

[source, Kotlin]
.Exemple :
----
import kotlin.math.PI
import kotlin.math.pow
----

Nous pouvons importer toutes les éléments d'un package en utilisant le symbole [red]*+++*+++*
On essaye d'utiliser le jocker * avec parcimonie. Cela n'aura aucun est sur la taille et perforamnce du code compilé, par contre la liste de suggestion de votre IDE va s'enrichir d'éléments par forcément utiles.

[source, Kotlin]
.Exemple :
----
import kotlin.math.*
----

WARNING: L'instruction d'import s'applique uniquement au package concerné et exclu les sous-packages du package faisant l'objet de l'import *.

=== Les alias - Renommer l'espace de nom
Vous pouvez renommer, il faut pour cela utiliser le mot clé [red]*as*

[source, Kotlin]
.Exemple :
----
import kotlin.math.pow as puissance
----